﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public GameObject particles;

    public Stat damage;

    void Awake()
    {
        currentHealth = maxHealth;
    }

    public virtual void TakeDamage(int damage)
    {
        currentHealth -= damage;
        GameManager.instnace.UpdateHealthBar();
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        PlayerManager.instance.player.GetComponent<PlayerController>().enemiesInGame.Remove(this.gameObject);
        GameObject pe = Instantiate(particles, transform.position, transform.rotation);
        Destroy(pe, 0.3f);
        Destroy(this.gameObject);
    }
}
