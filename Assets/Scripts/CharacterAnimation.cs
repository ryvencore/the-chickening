﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimation : MonoBehaviour
{
    const float locomationAnimationSmoothTime = 0.1f;

    NavMeshAgent agent;
    protected Animator animator;
    protected CharacterCombat combat;
    public bool whiteChicken = false;
    public float speedPercent;
    public bool pickedUp = false;

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        if (!whiteChicken)
        {
            combat = GetComponent<CharacterCombat>();
            combat.OnAttack += OnAttack;
        }
    }

    void Update()
    {
        if (!pickedUp)
        {
            speedPercent = agent.velocity.magnitude / agent.speed;
            animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);
        }
        else
        {
            speedPercent = 0f;
            animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);
        }
    }

    protected virtual void OnAttack()
    {
        animator.SetTrigger("attack");
    }
}
