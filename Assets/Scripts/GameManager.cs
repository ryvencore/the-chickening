﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instnace;

    [Header("Prefabs")]
    public GameObject[] chickens;
    [Header("Player HP")]
    public float curPlayerHP;
    public float maxPlayerHP;
    public GameObject player;
    public Image hpBar;
    [Header("Score")]
    public float curScore;
    public float maxScore;
    public Text scoreText;

    public enum GamePhase
    {
        INTRO,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        LOSE,
        OUTRO
    }
    public GamePhase curPhase;
    [System.NonSerialized]
    private float spawnDelay;
    private bool spawning = false;
    [System.NonSerialized]
    private float currentTime;
    public bool respawn;

    public GameObject[] greySpawners;
    public GameObject[] whiteSpawners;
    public GameObject[] greenSpawners;
    [System.NonSerialized]
    private string[] otherBoyText =
        {
        "Get those chickens!",
        "You can do it!",
        "Uhh, Satan!",
        "Woohoo!",
        "I see chicken, inside the fire",
        "This is better than last time!",
        "Chicken dinner?",
        "MORE! MORE! MORE!"
    };
    public GameObject textPanel;
    public Text talkText;
    public GameObject introPanel;
    public GameObject outroPanel;
    public bool outroStarted = false;
    public GameObject gameOverPanel;

    public List<GameObject> whiteChickList = new List<GameObject>();
    public List<GameObject> greenChickList = new List<GameObject>();

    void Start()
    {
        curPhase = GamePhase.INTRO;
        instnace = this;
        player = PlayerManager.instance.player;
        maxPlayerHP = player.GetComponent<PlayerStats>().maxHealth;
        curPlayerHP = 100;
        UpdateScore();
        currentTime = 3f;
        Debug.Log(currentTime);
        textPanel.SetActive(false);
        StartCoroutine(Intro());

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadSceneAsync(0);
        }

        if (curPhase != GamePhase.INTRO && curPhase != GamePhase.OUTRO && curPhase != GamePhase.LOSE)
        {
            ChickSpawner();
        }

        if (curScore <= 19 && curPhase != GamePhase.INTRO)
        {
            curPhase = GamePhase.ONE;
        }
        else if (curScore >= 20 && curScore <= 39)
        {
            curPhase = GamePhase.TWO;
        }
        else if (curScore >= 40 && curScore <= 59)
        {
            curPhase = GamePhase.THREE;
        }
        else if (curScore >= 60 && curScore <= 79)
        {
            curPhase = GamePhase.FOUR;
        }
        else if (curScore >= 80 && curScore <= 100)
        {
            curPhase = GamePhase.FIVE;
        }
        if (curScore >= 100)
        {
            curPhase = GamePhase.OUTRO;
        }

        switch (curPhase)
        {
            case (GamePhase.INTRO):

                break;
            case (GamePhase.ONE):
                spawnDelay = 15f;
                break;
            case (GamePhase.TWO):
                spawnDelay = 12f;
                break;
            case (GamePhase.THREE):
                spawnDelay = 10f;
                break;
            case (GamePhase.FOUR):
                spawnDelay = 8f;
                break;
            case (GamePhase.FIVE):
                spawnDelay = 5f;
                break;
            case (GamePhase.OUTRO):
                if (!outroStarted)
                {
                    StartCoroutine(Outro());
                }

                break;
            case (GamePhase.LOSE):

                break;
        }
    }

    public void UpdateHealthBar()
    {
        curPlayerHP = player.GetComponent<PlayerStats>().currentHealth;
        hpBar.fillAmount = curPlayerHP / maxPlayerHP;
        if (curPlayerHP > 100)
        {
            curPlayerHP = 100;
        }
    }
    public void UpdateScore()
    {
        scoreText.text = curScore.ToString();
        if (curScore < 0)
        {
            curScore = 0;
        }
    }

    void ChickSpawner()
    {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            spawning = true;
            if (spawning)
            {
                SpawnGreys();
                SpawnWhites();
                SpawnGreens();
            }
        }
    }

    void SpawnGreys()
    {
        if (player.GetComponent<PlayerController>().enemiesInGame.Count <= 30)
        {
            //Spawn greys
            for (int i = 0; i < greySpawners.Length; i++)
            {
                GameObject chicken = Instantiate(chickens[0], greySpawners[i].transform.position, Quaternion.identity);
                player.GetComponent<PlayerController>().enemiesInGame.Add(chicken);
            }
        }
        currentTime = spawnDelay;
        spawning = false;

    }
    void SpawnWhites()
    {
        if (whiteChickList.Count <= 20)
        {
            //Spawn whites
            for (int i = 0; i < whiteSpawners.Length; i++)
            {
                GameObject chicken = Instantiate(chickens[1], whiteSpawners[i].transform.position, Quaternion.identity);
                whiteChickList.Add(chicken);
            }
        }
    }
    void SpawnGreens()
    {
        if (greenChickList.Count <= 20)
        {
            //Spawn whites
            for (int i = 0; i < whiteSpawners.Length; i++)
            {
                GameObject chicken = Instantiate(chickens[2], greenSpawners[i].transform.position, Quaternion.identity);
                greenChickList.Add(chicken);
            }
        }
    }

    IEnumerator TalkStuff()
    {
        yield return new WaitForSeconds(9f);
        textPanel.SetActive(true);
        talkText.text = otherBoyText[Random.Range(0, otherBoyText.Length)];
        yield return new WaitForSeconds(3f);
        textPanel.SetActive(false);
        StartCoroutine(TalkStuff());
    }

    IEnumerator Intro()
    {
        introPanel.GetComponent<Animator>().SetTrigger("intro");
        yield return new WaitForSeconds(10f);
        Debug.Log("intro ends");
        curPhase = GamePhase.ONE;
        StartCoroutine(TalkStuff());
    }
    IEnumerator Outro()
    {
        outroStarted = true;
        player.GetComponent<PlayerStats>().currentHealth = 1000;
        outroPanel.GetComponent<Animator>().SetTrigger("outro");
        yield return new WaitForSeconds(12f);
        Debug.Log("outro ends");
        SceneManager.LoadSceneAsync(0);
    }

    public void Skip()
    {
        StopCoroutine(Intro());
        StopCoroutine(Intro());
        introPanel.SetActive(false);
        curPhase = GamePhase.ONE;
    }

    public void ToMenuButton()
    {
        SceneManager.LoadSceneAsync(0);
    }

}
