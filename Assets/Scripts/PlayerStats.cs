﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    public override void Die()
    {
        if (GameManager.instnace.curPhase != GameManager.GamePhase.OUTRO)
        {
            GameManager.instnace.curPhase = GameManager.GamePhase.LOSE;
            GameManager.instnace.gameOverPanel.SetActive(true);
        }
    }
}
