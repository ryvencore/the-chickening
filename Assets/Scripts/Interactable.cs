﻿using UnityEngine;
using UnityEngine.AI;

public class Interactable : MonoBehaviour
{
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    public float radius = 3f;
    public Transform interactionTransform;
    bool isFocus = false;
    public Transform player;
    public bool inRange = false;
    public GameObject indicator;

    bool hasInteracted = false;

    public float wanderRadius;
    public float wanderTimer;
    private Transform target;
    private NavMeshAgent agent;
    private float timer;

    public bool greenChicken = false;

    public virtual void Interact()
    {
        Debug.Log("Interacting with " + transform.name);
    }
    void Start()
    {
        player = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        indicator = transform.GetChild(0).gameObject;
        indicator.SetActive(false);
        timer = wanderTimer;

    }

    void Update()
    {
        if (!player.GetComponent<PlayerController>().throwing)
        {
            ChickenAction();
        }
    }

    public void ChickenAction()
    {
        if (!isFocus)
        {
            Wander();
        }
        float distance = Vector3.Distance(player.position, interactionTransform.position);
        if (distance <= radius)
        {
            indicator.SetActive(true);
            inRange = true;
            if (Input.GetKeyDown(KeyCode.E) && !isFocus)
            {
                indicator.SetActive(false);
                transform.parent = player.gameObject.transform.GetChild(0);
                GetComponent<BoxCollider>().enabled = false;
                player.GetComponent<PlayerController>().SetFocus(this);
                player.GetComponent<PlayerController>().hasChicken = true;
            }
            else if (Input.GetKeyDown(KeyCode.E) & isFocus && hasInteracted)
            {
                player.GetComponent<PlayerController>().RemoveFocus();

            }
        }
        else
        {
            inRange = false;
            indicator.SetActive(false);
        }

        if (isFocus && !hasInteracted)
        {
            if (distance <= radius)
            {
                hasInteracted = true;
            }
        }
        if (isFocus)
        {
            transform.position = player.transform.GetChild(0).transform.position;
            transform.rotation = player.transform.rotation;
            indicator.SetActive(false);
            GetComponent<CharacterAnimation>().pickedUp = true;
            GetComponent<CharacterAnimation>().speedPercent = 0f;
        }
    }

    public void OnFocused(Transform playerTransform)
    {
        isFocus = true;
        //player = playerTransform;
        hasInteracted = false;
    }
    public void OnDeFocused()
    {
        isFocus = false;
        //player = null;
        hasInteracted = false;
        transform.parent = null;
        GetComponent<CharacterAnimation>().pickedUp = false;
    }

    void Wander()
    {
        Debug.Log("wandering");
        timer += Time.deltaTime;

        if (timer >= wanderTimer)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            agent.SetDestination(newPos);
            timer = 0;
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }

}
