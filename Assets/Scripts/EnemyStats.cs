﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyStats : CharacterStats
{
    public Image hpBar;
    private NavMeshAgent agent;
    public Color _color;
    private Color local_color;
    private Renderer _renderer;

    void Start()
    {
        _renderer = transform.GetChild(1).GetComponent<Renderer>();
        agent = GetComponent<NavMeshAgent>();
        local_color = _renderer.material.color;
    }

    void Update()
    {
        hpBar.transform.LookAt(Camera.main.transform.position, Camera.main.transform.up);
    }

    public override void Die()
    {
        base.Die();
        //kill enemy
    }
    public override void TakeDamage(int damage)
    {
        currentHealth -= damage;
        hpBar.fillAmount = (float)currentHealth / (float)maxHealth;

        if (currentHealth <= 0)
        {
            PlayerManager.instance.player.GetComponent<PlayerController>().enemiesInGame.Remove(this.gameObject);
            GameManager.instnace.curScore -= 2;
            GameManager.instnace.UpdateScore();
            Die();
        }
        StartCoroutine(PushBack());
    }
    IEnumerator PushBack()
    {
        _renderer.material.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        _renderer.material.color = local_color;
    }
}
