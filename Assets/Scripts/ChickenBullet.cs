﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenBullet : MonoBehaviour
{
    public Rigidbody rb;
    public int throwForce = 30;
    public GameObject player;
    public GameManager GM;
    public Vector3 dir;
    public bool greenChicken;
    public GameObject particles;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GM = GameManager.instnace;
        player = PlayerManager.instance.player;
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), player.transform.GetComponent<Collider>());
        dir = player.transform.rotation * Vector3.forward;
    }

    void Update()
    {

        //rb.AddForce(Vector3.forward, ForceMode.Impulse);
        transform.Translate(dir * throwForce * Time.deltaTime);
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.transform.name);
        if (other.transform.tag == "Bonefire")
        {
            GM.curScore += 5;
            GM.UpdateScore();
            if (greenChicken)
            {
                player.GetComponent<PlayerStats>().currentHealth += 20;
                GM.UpdateHealthBar();
            }
            GameObject pe = Instantiate(particles, transform.position, transform.rotation);
            Destroy(pe, 0.3f);
            Destroy(this.gameObject);
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("Wall") || other.gameObject.layer == LayerMask.NameToLayer("NPC"))
        {
            GameObject pe = Instantiate(particles, transform.position, transform.rotation);
            Destroy(pe, 0.3f);
            Destroy(this.gameObject);
        }
        if (other.transform.tag == "Enemy")
        {
            GameObject pe = Instantiate(particles, transform.position, transform.rotation);
            Destroy(pe, 0.3f);
            other.transform.GetComponent<EnemyStats>().TakeDamage(10);
            Destroy(this.gameObject);
        }
    }
}
