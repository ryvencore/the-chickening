﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    const float locomationAnimationSmoothTime = 0.1f;

    CharacterController controller;
    protected Animator animator;

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        PlayerManager.instance.player.GetComponent<PlayerController>().OnPlayerAttack += OnPlayerAttack;
    }

    void Update()
    {
        float speedPercent = controller.velocity.magnitude / 10f;
        animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);
    }

    protected virtual void OnPlayerAttack()
    {
        animator.SetTrigger("attack");
    }

}
