﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody _rb;
    public Interactable focus;
    private float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public event System.Action OnPlayerAttack;
    private CharacterController controller;
    public bool hasChicken = false;
    public GameObject attackZone;
    public bool isAttacking = false;
    public GameObject chickenBullet;
    public GameObject greenChickenBullet;

    public float attackCooldown = 0f;
    public float attackDelay = 0f;
    public float attackSpeed = 1f;
    public bool throwing = false;

    public List<GameObject> enemiesInGame = new List<GameObject>();


    void Start()
    {
        controller = GetComponent<CharacterController>();

    }
    void Update()
    {
        attackCooldown -= Time.deltaTime;
        if (GameManager.instnace.curPhase != GameManager.GamePhase.INTRO || GameManager.instnace.curPhase != GameManager.GamePhase.OUTRO || GameManager.instnace.curPhase != GameManager.GamePhase.LOSE)
        {
            Move();
            Attack();
            ThrowChicken();
        }
    }

    void Move()
    {
        float cordX = Input.GetAxis("Horizontal");
        float cordZ = Input.GetAxis("Vertical");

        if (controller.isGrounded)
        {
            moveDirection = new Vector3(cordX, 0.0f, cordZ);
            if (moveDirection != Vector3.zero) transform.rotation = Quaternion.LookRotation(moveDirection);
            moveDirection = moveDirection * speed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !hasChicken)
        {
            if (attackCooldown <= 0f)
            {
                speed = 0f;
                isAttacking = true;
                StartCoroutine(DoDamage(attackDelay));
                RemoveFocus();
                if (OnPlayerAttack != null)
                {
                    OnPlayerAttack();
                }
                attackCooldown = 1f / attackSpeed;
            }
        }
    }

    IEnumerator DoDamage(float delay)
    {
        yield return new WaitForSeconds(delay);
        for (int i = 0; i < enemiesInGame.Count; i++)
        {
            if (enemiesInGame[i].GetComponent<EnemyController>().inAttackRange)
            {
                enemiesInGame[i].GetComponent<EnemyStats>().TakeDamage(GetComponent<PlayerStats>().damage.GetValue());
            }
        }
        speed = 10f;
        isAttacking = false;
    }

    public void SetFocus(Interactable newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
            {
                focus.OnDeFocused();
            }
            focus = newFocus;
        }
        newFocus.OnFocused(transform);
    }

    public void RemoveFocus()
    {
        if (focus != null)
        {
            focus.OnDeFocused();
        }
        focus = null;
        hasChicken = false;
    }

    void ThrowChicken()
    {
        if (hasChicken)
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject chicken = focus.gameObject;
                if (!chicken.GetComponent<Interactable>().greenChicken)
                {
                    throwing = true;
                    RemoveFocus();
                    GameManager.instnace.whiteChickList.Remove(chicken);
                    Destroy(chicken);
                    hasChicken = false;
                    GameObject _chickenBullet = Instantiate(chickenBullet, transform.position, Quaternion.identity);
                    throwing = false;
                }
                else if (chicken.GetComponent<Interactable>().greenChicken)
                {
                    throwing = true;
                    RemoveFocus();
                    GameManager.instnace.greenChickList.Remove(chicken);
                    Destroy(chicken);
                    hasChicken = false;
                    GameObject _chickenBullet = Instantiate(greenChickenBullet, transform.position, Quaternion.identity);
                    throwing = false;
                }
            }
        }
    }
}
